#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <numeric> //accumulate

int main()
{

    // to load file
    std::ifstream filein("day6/input.txt");
    std::vector<std::string> entry_vec;

    char delim = '\n\n';
    for (std::string line; std::getline(filein, line, delim);)
    {
        entry_vec.push_back(line);
    }

    std::vector<std::string> entry_vec_group{""};
    int idx = 0;
    for (int i = 0; i < entry_vec.size(); i++)
    {
        if (entry_vec[i].empty())
        {
            idx++;
        }
        if (!(entry_vec_group.size() == (idx + 1)))
        {
            entry_vec_group.push_back("");
        }
        if (!entry_vec_group[idx].empty())
        {
            entry_vec_group[idx].append(" ");
        }
        entry_vec_group[idx].append(entry_vec[i]);
    }

    //basic bitch approach
    std::vector<int> unique_counts;
    for (int i = 0; i < entry_vec_group.size(); i++)
    {
        // construct char vector
        std::vector<char> line_char(entry_vec_group[i].begin(), entry_vec_group[i].end());
        // construct char vector to test against
        const std::string abc = "abcdefghijklmnopqrstuvwxyz";
        const std::vector<char> abc_char(abc.begin(), abc.end());
        // construct counting vector
        std::vector<char> unique_elements;

        // check if line_char[i] contains any in abc_char, if so delete from abc
        for (int j = 0; j < abc_char.size(); j++)
        {
            auto p = std::find(line_char.begin(), line_char.end(), abc_char[j]);
            if (p != line_char.end())
            {
                // found abc[i] in line_char, add to unique set
                unique_elements.push_back(abc_char[j]);
            }
        }

        // find # of unique chars
        int number_unique = unique_elements.size();

        // push it
        unique_counts.push_back(number_unique);
        std::cout << number_unique << std::endl;
    }

    int sum = std::accumulate(unique_counts.begin(), unique_counts.end(), 0);

    std::cout << "answer is " << sum << std::endl;
    return 1;
}