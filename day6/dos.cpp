#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <numeric> //accumulate

int main()
{

    // to load file
    std::ifstream filein("day6/input.txt");
    std::vector<std::string> entry_vec;

    char delim = '\n\n';
    for (std::string line; std::getline(filein, line, delim);)
    {
        entry_vec.push_back(line);
    }

    std::vector<std::string> entry_vec_group{""};
    int idx = 0;
    for (int i = 0; i < entry_vec.size(); i++)
    {
        if (entry_vec[i].empty())
        {
            idx++;
        }
        if (!(entry_vec_group.size() == (idx + 1)))
        {
            entry_vec_group.push_back("");
        }
        if (!entry_vec_group[idx].empty())
        {
            entry_vec_group[idx].append(" ");
        }
        entry_vec_group[idx].append(entry_vec[i]);
    }
    std::vector<int> groups_matching;
    while(!groups_matching.empty())
    {
        groups_matching.erase(groups_matching.end());
    }

    // find # persons
    for (int i = 0; i < entry_vec.size(); i++)
    {
        int number_persons = 0;
        while (entry_vec[i + number_persons] != "")
        {
            number_persons++;
        }

        std::vector<char> check_vector(entry_vec[i].begin(), entry_vec[i].end());
        std::sort(check_vector.begin(),check_vector.end());
        for (int u = 0; u < number_persons; u++)
        {
            
            std::vector<char> next_check(entry_vec[i + u].begin(), entry_vec[i + u].end());
            std::sort(next_check.begin(),next_check.end());
            std::vector<char> new_matching;

            int size = check_vector.size();
            for(int y = 0; y <size; y++)
            {
                bool check = false;
                for(int j =0; j < next_check.size(); j++)
                {
                    if(check_vector[y]==next_check[j])
                    {
                        new_matching.push_back(check_vector[y]);
                    }
                }
            }
            check_vector = new_matching;
            //maybe only unique?
            // int size = check_vector.size();
            // for (int j = 0; j < size ; j++)
            // {
            //     //only keep common elements
            //     auto p = std::find(next_check.begin(), next_check.end(), check_vector[j]);
            //     if (p == next_check.end())
            //     {
            //         check_vector.erase(check_vector.begin()+j);
            //         size--;
            //     }
            //     else
            //     {
            //         //remove from matching
            //     }
            // }
        }



        int sum = check_vector.size();
        groups_matching.push_back(sum);
        std::cout << "this group has: " << sum << " same answers" << std::endl;

        i += number_persons;
    }
    std::cout << "sum of all matches: " << std::accumulate(groups_matching.begin(), groups_matching.end(), 0);

    return 1;
}