#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <regex>
#include <set>

std::vector<std::string> split(const std::string &input, const std::string &regex)
{
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    std::sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;
    return {first, last};
}

int main()
{
    // to load file
    std::ifstream filein("day4/input.txt");
    std::vector<std::string> entry_vec;

    char delim = '\n\n';
    for (std::string line; std::getline(filein, line, delim);)
    {
        entry_vec.push_back(line);
    }

    std::vector<std::string> entry_vec_group{""};
    int idx = 0;
    for (int i = 0; i < entry_vec.size(); i++)
    {
        if (entry_vec[i].empty())
        {
            idx++;
        }
        if (!(entry_vec_group.size() == (idx + 1)))
        {
            entry_vec_group.push_back("");
        }
        if (!entry_vec_group[idx].empty())
        {
            entry_vec_group[idx].append(" ");
        }
        entry_vec_group[idx].append(entry_vec[i]);
    }

    //split
    std::string entry_separator = " ";
    std::string field_separator = ":";

    std::vector<std::vector<std::string>> data;

    for (int j = 0; j < entry_vec_group.size(); j++)
    {
        std::vector<std::string> separated_entries;

        size_t pos = 0;
        std::string s = entry_vec_group[j];
        std::string token;

        while ((pos = s.find(entry_separator)) != std::string::npos)
        {
            token = s.substr(0, pos);
            s.erase(0, pos + entry_separator.length());

            separated_entries.push_back(token);
        }
        separated_entries.push_back(s);

        data.push_back(separated_entries);
    }

    int good_passes = 0;
    for (int i = 0; i < data.size(); i++)
    {
        std::vector<std::vector<std::string>> current_row;
        for (int j = 0; j < data[i].size(); j++)
        {
            current_row.push_back(split(data[i][j], ":"));
        }

        int current_password_counter = 0;
        bool daar_komt_cid = false;

        for (int y = 0; y < current_row.size(); y++)
        {
            
            if (current_row[y][0].compare("byr") == 0)
            {
                if(std::regex_match(current_row[y][1],std::regex("^(19[2-9]\\d|200[0-2])$")))
                {
                    current_password_counter++;
                }
            }
            else if (current_row[y][0].compare("iyr") == 0)
            {
                if(std::regex_match(current_row[y][1],std::regex("^(201[0-9]|2020)$")))
                {
                    current_password_counter++;
                }
            }
            else if (current_row[y][0].compare("eyr") == 0)
            {
                if(std::regex_match(current_row[y][1],std::regex("^(202[0-9]|2030)$")))
                {
                    current_password_counter++;
                }
            }
            else if (current_row[y][0].compare("hgt") == 0)
            {
                if(std::regex_match(current_row[y][1],std::regex("^(1[5-8][0-9]cm|19[0-3]cm)|(59in|6\\din|7[0-6]in)$")))
                {
                    current_password_counter++;
                }
            }
            else if (current_row[y][0].compare("hcl") == 0)
            {
                if(std::regex_match(current_row[y][1],std::regex("#[0-9a-f]{6}")))
                {
                    current_password_counter++;
                }
            }
            else if (current_row[y][0].compare("ecl") == 0)
            {
                if(std::regex_match(current_row[y][1],std::regex("(amb)|(blu)|(brn)|(gry)|(grn)|(hzl)|(oth)")))
                {
                    current_password_counter++;
                }
            }
            else if (current_row[y][0].compare("pid") == 0)
            {
                if(std::regex_match(current_row[y][1],std::regex("[0-9]{9}")))
                {
                    current_password_counter++;
                }
            }
            else if (current_row[y][0].compare("cid") == 0)
            {
                current_password_counter++;
                daar_komt_cid = true;

            }
        }
        if(daar_komt_cid)
        {
            if(current_password_counter == 8)
            {
                good_passes++;
            }
        }
        else
        {
            if(current_password_counter == 7)
            {
                good_passes++;
            }
        }
        
        
        
        
    }
    std::cout << "# of valid passes: " << good_passes << std::endl;
    return 0;
}
