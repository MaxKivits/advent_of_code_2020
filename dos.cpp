#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <regex>

std::vector<std::string> split(const std::string &input, const std::string &regex)
{
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    std::sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;
    return {first, last};
}
bool replace(std::string &str, const std::string &from, const std::string &to)
{
    size_t start_pos = str.find(from);
    if (start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

class instruction
{
public:
    std::string type;
    int value;
    int executed = 0;
    int switched = 0;
    instruction(const std::string &type_, const int &value_) : type(type_), value(value_)
    {
    }
    void execute(int & accumulator, int & stack_index)
    {
        executed++;
        if(type.compare("nop")==0)
        {
            stack_index++;
        }
        else if(type.compare("acc")==0)
        {
            accumulator += value;
            stack_index++;
        }
        else if(type.compare("jmp")==0)
        {
            stack_index+=value;
        }
    }
    std::string toString()
    {
        return type+std::to_string(value);
    }
    void alternate()
    {
        switched++;
        if(type.compare("nop")==0)
        {
            type = "jmp";
        }
        else if(type.compare("jmp")==0)
        {
            type = "nop";
        }
    }
};

int main()
{

    std::vector<instruction> instructions;
    // std::ifstream input_stream("day8/input.txt");
    std::ifstream input_stream("day8/input.txt");

    int accumulator = 0;
    int stack_index = 0;

    
    for (std::string line; std::getline(input_stream, line, '\n');)
    {
        auto line_split = split(line, " ");
        replace(line_split[1], "+", "");
        instruction current_instruction(line_split[0],std::stoi(line_split[1]));
        instructions.push_back(current_instruction);
    }
    
    int size = instructions.size();
    bool found = false;
    
    
    for(int i = 0; i < size; i++)
    {
        // reset all
        for(auto & x: instructions)
        {
            x.executed = 0;
        }
        accumulator = 0;
        stack_index = 0;
        
        // flip current instructions
        instructions[i].alternate();
        

        std::cout << "outer loop number " <<  i << std::endl;

        if(i == 7){
            std::cout << "Hello2" << std::endl;
        }
        // execute untill something runs twice or stack index = 651
        for( ; 1 ;)
        {
            if(stack_index >= instructions.size())
            {
            found = true;
            std::cout << "END REACHED!" << std::endl;
            break;
            }

            if(instructions[stack_index].executed==0)
            {
                // std::cout << "excecuting: " << instructions[stack_index].toString() <<std::endl;
                
                instructions[stack_index].execute(accumulator,stack_index);
            }

            else
            {   
                break;
            }
            
            
            
        }
        if(found)
        {
            std::cout << " thank gods " << std::endl;
            break;
        }

        //flip back
        instructions[i].alternate();

    }

    std::cout << "we got here" << std::endl;
    std::cout << accumulator << std::endl;

    return 1;
}