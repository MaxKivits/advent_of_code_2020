
#include <string>
#include <fstream>
#include <vector>
#include <utility>
#include <iostream>
#include <stdexcept> // std::runtime_error
#include <sstream>   // std::stringstream
#include <algorithm>


int main()
{

    std::ifstream input_file("input.csv");

    std::string lijn;
    std::vector<std::string> data;

    std::string max, filter, password;


    int password_count =0;


    while (input_file >> lijn)
    {
        data.push_back(lijn);

        std::stringstream ss(lijn);
        std::vector<std::string> parsed_line;

        while(ss.good())
        {
            std::string substr;
            getline(ss,substr, ',');
            parsed_line.push_back(substr);
        }
        /* for(const auto & i : parsed_line)
        {
            std::cout << i << std::endl;
        } */

        std::stringstream ss2(parsed_line[0]);
        std::vector<int> min_max;

        while (ss2.good())
        {
            std::string substr;
            getline(ss2,substr,'-');
            min_max.push_back(std::stoi(substr,nullptr));
        }
        
        // std::string search_str = parsed_line[1];

        std::vector<char> passwrd (parsed_line[2].begin(),parsed_line[2].end());
        bool check1, check2 = 0;
        check1 = passwrd[min_max[0]-1] == *parsed_line[1].c_str();
        check2 = passwrd[min_max[1]-1] == *parsed_line[1].c_str() ;
        if(check1 != check2)
        {
            password_count++;
        }

        std::cout<<"debug"<<std::endl;

    }


    std::cout << "total lines: " << data.size() << "\n" << "total valid passwords: " << password_count << std::endl;

    return 1;
}