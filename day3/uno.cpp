#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>

#include <iterator>
#include <algorithm>

int main()
{
    // to load file
    std::ifstream file_stream("day3/input.txt");
    std::ostringstream ss;
    std::string lijn;
    std::vector<std::vector<char>> forest_char;

    // read file line by line and push char vector into forest_char
    while (file_stream >> lijn)
    {
        forest_char.push_back(std::vector<char>(lijn.begin(), lijn.end()));
    }

    // calc size
    int size_y = forest_char.size();
    int size_x = forest_char[0].size();
    std::cout <<  "width = " << size_x << "  height = " << size_y << std::endl;

    int slope_x = 3;
    int slope_y = 1;
    int xIdx = 0;
    int yIdx = 0;
    int le_tree_counter = 0;


    for(int y = 1; y<size_y ; y++)
    {
        xIdx = (xIdx+slope_x)%size_x;
        yIdx += slope_y;
        // std::cout << "x pos: " << xIdx << "  y pos: " << yIdx << std::endl;
        // std::cout << forest_char[yIdx][xIdx] << std::endl;

        if(forest_char[yIdx][xIdx] == '#')
        {
            le_tree_counter++;
        }

    }


    // zieke vector output line
    // std::copy(line_chars.begin(), line_chars.end(), std::ostream_iterator<char>(std::cout));
    std::cout << "we encountered: " << le_tree_counter << " trees" <<  std::endl;

    return 0;
}