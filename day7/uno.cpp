#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <regex>
#include <set>
#include <memory>


std::vector<std::string> split(const std::string &input, const std::string &regex)
{
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    std::sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;
    return {first, last};
}
bool replace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

class bag
{
    public:
    std::string color;
    std::vector<bag*> pointsTo;
    bag* pointsFrom;
    struct Finder
    {
        Finder(std::string const & n) : name(n) {}
        bool operator () (const bag & el) const
        {
            return el.color == name;
        }
        std::string name;
    };
};


int main()
{
    std::vector<bag> baglist;

    // to load file
    std::ifstream filein("day7/input.txt");
    std::vector<std::vector<std::string>> data;
    std::vector<std::string> line_split;


    char delim = '\n';
    for (std::string line; std::getline(filein, line, delim);)
    {
        replace(line, " contain", ",");
        replace(line, " bags", "");
        while(replace(line, " bag", "")){};
        line_split = split(line,", ");

        for(const auto & i: baglist)
        {
            if(i.color.compare(line_split[0])!=0)
            {
                
            }
            else
            {

            }
            
        }
        for(int i = 0; i<baglist.size(); i++)
        {
            
        }




        data.push_back(line_split);


    }

    
    
    
    return 1;
}