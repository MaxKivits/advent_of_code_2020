#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <regex>

std::vector<std::string> split(const std::string &input, const std::string &regex)
{
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    std::sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;
    return {first, last};
}
bool replace(std::string &str, const std::string &from, const std::string &to)
{
    size_t start_pos = str.find(from);
    if (start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

class instruction
{
public:
    std::string type;
    int value;
    int executed = 0;
    instruction(const std::string &type_, const int &value_) : type(type_), value(value_)
    {
    }
    void execute(int & accumulator, int & stack_index)
    {
        executed++;
        if(type.compare("nop")==0)
        {
            stack_index++;
        }
        else if(type.compare("acc")==0)
        {
            accumulator += value;
            stack_index++;
        }
        else if(type.compare("jmp")==0)
        {
            stack_index+=value;
        }
    }
};

int main()
{
    // load input
    std::vector<instruction> instructions;
    std::ifstream input_stream("day8/input.txt");

    int accumulator = 0;
    int stack_index = 0;

    for (std::string line; std::getline(input_stream, line, '\n');)
    {
        auto line_split = split(line, " ");
        replace(line_split[1], "+", "");
        instruction current_instruction(line_split[0],std::stoi(line_split[1]));
        instructions.push_back(current_instruction);
    }

    int lit_index = 0;
    // execute
    for(int i =0; instructions.size();i++) 
    {   
        // change i-th instruction if it is jmp or nop
        if(instructions[i].type.compare("nop")==0 )
        {
            instructions[i].type = "jmp";
        }
        else if(instructions[i].type.compare("jmp")==0 )
        {
            instructions[i].type = "nop";
        }

        for( ; stack_index<instructions.size() ;)
        {
            if(instructions[stack_index].executed==0)
            {

                instructions[stack_index].execute(accumulator,stack_index);
                if(stack_index >= instructions.size())
                {
                    lit_index = i;
                }
            }
            else
            {
                stack_index = 0;
                for(auto & u : instructions)
                {
                    u.executed = 1;
                }
                break;
            }
            
        }

    }


    std::cout << lit_index << std::endl;

    return 1;
}