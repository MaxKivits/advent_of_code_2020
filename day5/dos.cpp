
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>

#include <iterator>
#include <cstring>
#include <algorithm>

int main()
{

    // to load file
    std::ifstream file_stream("day5/input.txt");
    std::ostringstream ss;
    std::string lijn;
    std::vector<std::vector<char>> char_vec;
    std::vector<int> row;

    // read file line by line and push char vector into forest_char
    while (file_stream >> lijn)
    {
        std::vector<char> line_char (lijn.begin(), lijn.end());        

        std::replace(line_char.begin(),line_char.end()-3,'B','1');
        std::replace(line_char.begin(),line_char.end()-3,'F','0');
        std::replace(line_char.begin()+7,line_char.end(),'R','1');
        std::replace(line_char.begin()+7,line_char.end(),'L','0');


        // std::memcpy(&row,row_char,8);


        std::string megastring (line_char.begin(),line_char.end());
        row.push_back(std::stoi(megastring,nullptr,2));






    }


    std::sort(row.begin(),row.end());

    
    std::vector<int> missing_values;

    for(int i = 0; i<row.size()-1; i++)
    {
        if((row[i+1]-row[i])!=1)
        {
            missing_values.push_back(row[i]+1);
        }
    }


    return 1;
}