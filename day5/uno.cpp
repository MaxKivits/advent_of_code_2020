
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>

#include <iterator>
#include <cstring>
#include <algorithm>

int main()
{

    // to load file
    std::ifstream file_stream("day5/input.txt");
    std::ostringstream ss;
    std::string lijn;
    std::vector<std::vector<char>> char_vec;
    int row = 0;
    int maxrow = 0;

    // read file line by line and push char vector into forest_char
    while (file_stream >> lijn)
    {
        std::vector<char> line_char (lijn.begin(), lijn.end());        

        std::replace(line_char.begin(),line_char.end()-3,'B','1');
        std::replace(line_char.begin(),line_char.end()-3,'F','0');
        std::replace(line_char.begin()+7,line_char.end(),'R','1');
        std::replace(line_char.begin()+7,line_char.end(),'L','0');


        // std::memcpy(&row,row_char,8);


        std::string megastring (line_char.begin(),line_char.end());
        row = std::stoi(megastring,nullptr,2);

        if(row > maxrow)
        {
            maxrow = row;
        }





    }

    
    std::cout << maxrow << std::endl;



    return 1;
}