#include <algorithm>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <deque>
#include <numeric>

bool isSumOfList(const long long int &checkee, const std::deque<long long int> &list)
{
    for (const auto &i : list)
    {
        for (const auto &j : list)
        {
            if (i + j == checkee)
            {
                return true;
            }
        }
    }
    return false;
}

bool checkIfSetIsGay(const long long int &checkee, const std::vector<long long int> &set)
{
    return (checkee == std::accumulate(set.begin(),set.end(),0));
}

int main(void)
{

    std::vector<long long int> data;
    std::ifstream input_stream("day9/input.txt");

    for (std::string line; std::getline(input_stream, line, '\n');)
    {
        data.push_back(std::stoll(line));
    }

    int preamble_size = 25;
    std::deque<long long int> sliding_window;

    long long int gay_int = 0;
    bool correct = true;

    for (int i = 0; i < data.size(); i++)
    {
        gay_int = data[i];
        if (i < preamble_size)
        {
            //preamble
            sliding_window.push_back(gay_int);
        }
        else if (sliding_window.size() == preamble_size)
        {
            // entry check
            if (!isSumOfList(gay_int, sliding_window))
            {
                correct = false;
                break;
            }
            else
            {
                sliding_window.push_back(gay_int);
                sliding_window.pop_front();
            }
        }
        else
        {
            std::cout << "SHOULD NEVER GET HERE" << std::endl;
            return 0;
        }
    }
    if (!correct)
    {
        std::cout << gay_int << " is fake and gay\n"
                  << std::endl;
    }

    //now find contigous set that sums to the gay number
    bool set_found = false;
    std::vector<long long int> try_set;
    for (int set_size = 2; set_size < data.size(); set_size++)
    {
        //iterate over all data elements
        for (int i_data = 0; i_data < data.size() - set_size; i_data++)
        {
            //create set size set_size
            try_set.clear();
            for (int ifill = 0; ifill < set_size; ifill++)
            {
                try_set.push_back(data[i_data + ifill]);
            }
            if (checkIfSetIsGay(gay_int, try_set))
            {
                set_found = true;
                break;
            }
        }
        if (set_found)
        {
            break;
        }
    }
    if (set_found)
    {
        std::cout << "gay set is:" << std::endl;
        for (const auto &print_i : try_set)
        {
            std::cout << print_i << std::endl;
        }

        // long long int sum = *try_set.begin() + *try_set.end();
        
        auto test = std::minmax_element(try_set.begin(),try_set.end());
        auto big = std::get<0>(test);
        auto small = std::get<1>(test);
        // + std::get<1>(test); 
        // long long int sum = *test[0]+*test[1];

        // std::cout << "sum of first and last element in the set is:\n" << std::endl; 
        std::cout<<"de som mag je zelf doen haha! (fuck std::iterators en longlongdong)" << std::endl;
        // std::cout << "sum of first and last element in the set is:\n" << sum << std::endl; 
    }
    else
    {
        std::cout << "no set found" << std::endl;
    }

    return 1;
}