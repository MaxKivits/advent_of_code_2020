#include<vector>
#include<iostream>
#include<string>
#include<fstream>
#include<deque>


bool isSumOfList (const long long int &checkee, const std::deque<long long int> &list)
{
    for(const auto & i: list)
    {
        for(const auto & j : list)
        {
            if(i+j == checkee)
            {
                return true;
            }
        }
    }
    return false;
}

int main(void)
{


    std::vector<long long int> data;
    std::ifstream input_stream("day9/input.txt");


    for(std::string line; std::getline(input_stream,line,'\n');)
    {
        data.push_back(std::stoll(line));
    }

    int preamble_size = 25;
    std::deque<long long int> sliding_window;

    long long int current_int = 0;
    bool correct = true;

    for(int i=0;i<data.size();i++)
    {
        current_int = data[i];
        if(i<preamble_size)
        {
            //preamble
            sliding_window.push_back(current_int);
        }
        else if(sliding_window.size() == preamble_size)
        {
            // entry check
            if(!isSumOfList(current_int,sliding_window))
            {
                correct = false;
                break;
            }
            else
            {
                sliding_window.push_back(current_int);
                sliding_window.pop_front();
            }
            

        }
        else
        {
            std::cout<<"SHOULD NEVER GET HERE"<<std::endl;
            return 0;
        }
        

       
    }
    if(!correct)
    {
        std::cout <<current_int<< " is fake and gay" << std::endl;
    }

    
    return 1;
}